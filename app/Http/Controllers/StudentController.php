<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::with('group')->get();
        return response()->json($students);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|regex:/^[A-Za-z]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z]+$/|max:255',
            'group_id' => 'required|exists:groups,id',
            'birthday' => 'required|string',
            'gender' => 'required|string|in:M,F',
        ]);


        $student = new Student();
        $student->fill($validatedData);
        $student->save();

        return response()->json($student);
    }


    public function show($id)
    {
        $student = Student::with('group')->find($id);
        if (!$student) {
            return response()->json(['message' => 'Student not found'], 404);
        }
        return response()->json($student);
    }

    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        if (!$student) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $validatedData = $request->validate([
            'first_name' => 'required|regex:/^[A-Za-z]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Za-z]+$/|max:255',
            'group_id' => 'required|exists:groups,id',
            'birthday' => 'required|string',
            'gender' => 'required|string|in:M,F',
        ]);

        $student->fill($validatedData);
        $student->save();

        return response()->json($student);
    }


    public function destroy(Request $request)
    {
        $validatedData = $request->validate([
            'ids' => 'required|array',
            'ids.*' => 'required|exists:students,id',
        ]);

        $deleted = Student::whereIn('id', $validatedData['ids'])->delete();

        return response()->json(['message' => "$deleted users deleted"]);
    }
}

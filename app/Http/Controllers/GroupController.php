<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Group;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        return response()->json($groups);
    }

    public function store(Request $request)
    {
        $group = new Group();
        $group->title = $request->input('title');
        $group->save();
        return response()->json($group);
    }

    public function show($id)
    {
        $group = Group::find($id);
        if (!$group) {
            return response()->json(['message' => 'Group not found'], 404);
        }
        return response()->json($group);
    }

    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        if (!$group) {
            return response()->json(['message' => 'Group not found'], 404);
        }
        $group->title = $request->input('title');
        $group->save();
        return response()->json($group);
    }

    public function destroy($id)
    {
        $group = Group::find($id);
        if (!$group) {
            return response()->json(['message' => 'Group not found'], 404);
        }
        $group->delete();
        return response()->json(['message' => 'Group deleted']);
    }
}

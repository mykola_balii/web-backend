<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['first_name', 'last_name', 'group_id', 'birthday', 'gender'];
    protected $hidden = ['created_at', 'updated_at'];
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}


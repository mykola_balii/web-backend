<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::apiResource('students', 'App\Http\Controllers\StudentController');
Route::put('students/{id}', 'App\Http\Controllers\StudentController@update');
Route::delete('students', 'App\Http\Controllers\StudentController@destroy');
Route::apiResource('groups', 'App\Http\Controllers\GroupController');
